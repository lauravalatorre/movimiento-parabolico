from math import *
from time import sleep

from graphics import *
vo=60
t=0
wo=45*3.1415926535/180

y=0
g=9.8

ventana=GraphWin('simulador',400,300)
'''para hacer una ventana'''

ventana.setCoords(0,0,400,300)
while(y>=0):
    x=vo*cos(wo)*t
    y=vo*sin(wo)*t-(1.0/2)*g*t*t
    micirculo=Circle(Point(x,y),10)
    '''circulo necesita x, y y diametro'''
    micirculo.draw(ventana)
    micirculo.setFill('green')
    print(x,y)
    sleep(0.1)
    t=t+0.1

ventana.getMouse()